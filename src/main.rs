mod abbrevs;

use std::{fs::File, io::Read, path::PathBuf};

use abbrevs::abbrev_list;
use color_eyre::Result;

use clap::Parser;
use itertools::Itertools;
use nom_bibtex::{
    error::BibtexError,
    model::{KeyValue, StringValueType},
    Bibtex, Entry,
};
use regex::Regex;

#[derive(Parser)]
struct Cli {
    infile: PathBuf,
}

fn read_bibtex_file(file: PathBuf) -> Vec<Entry> {
    let files = vec![
        "vars/months.bib".into(),
        "/usr/share/texmf-dist/bibtex/bib/ieeetran/IEEEabrv.bib".into(),
        file,
    ];

    files
        .iter()
        .map(|filename| {
            let mut file = File::open(filename).expect(&format!("Failed to open {:?}", filename));
            let mut content = vec![];
            file.read_to_end(&mut content)
                .expect(&format!("Failed to read content of {:?}", filename));
            let file_content = String::from_utf8_lossy(&content);

            match Bibtex::raw_parse(&file_content) {
                Ok(entries) => entries,
                Err(BibtexError::Parsing(e)) => {
                    panic!("Parse error in {:?}:\n{}", filename, e)
                }
                Err(e) => Err(e).unwrap(),
            }
        })
        .fold(vec![], |mut acc, mut entries| {
            acc.append(&mut entries);
            acc
        })
}

fn main() -> Result<()> {
    color_eyre::install()?;
    let args = Cli::parse();

    let entries = read_bibtex_file(args.infile);

    let abbrevs = abbrev_list();

    let year_regex = Regex::new(r"\d{4}").unwrap();
    let compact_year_regex = Regex::new(r"'\d{2}").unwrap();
    let shorthand_regex = Regex::new(r"\(\{[A-z]+\}\)").unwrap();

    for entry in entries {
        match entry {
            Entry::Bibliography(_, entry_key, fields) => {
                for KeyValue { key, value } in fields {
                    if key == "booktitle" || key == "journal" {
                        let full_title = value
                            .iter()
                            .map(|val| match val {
                                StringValueType::Str(v) => v,
                                StringValueType::Abbreviation(v) => v,
                            })
                            .join("");

                        let mut modified = full_title.clone();

                        for word in full_title.split_whitespace() {
                            let w = word.to_lowercase();
                            if abbrevs.contains_key(w.as_str()) {
                                modified = modified.replace(word, abbrevs[w.as_str()]);
                            }

                            if year_regex.is_match(&w)
                                || compact_year_regex.is_match(&w)
                                || shorthand_regex.is_match(&w)
                            {
                                modified = modified.replace(&w, "");
                            }
                        }

                        if modified != full_title {
                            println!("{entry_key}");
                            println!("\t{full_title}");
                            println!("\t{modified}");
                        }
                    }
                }
            }
            _ => {}
        }
    }

    Ok(())
}
